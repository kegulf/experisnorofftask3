﻿using System;
using System.Collections.Generic;

/**
 * Task 3: Draw a Square
 * 
 * Write a program which can be used to print a square of size chosen by the user at
 * run-time.
 * The square can be made of any character that you choose. (# is probably a good
 * choice)
 * The file must compile without errors.example: (after compilation)
 * I use _ to show a space here(Yours should be blank)
 * 
 * #####
 * #___#
 * #___#
 * #___#
 * #####
 */

namespace ExperisNoroffTask3 {
    class Program {

        /// <summary>
        /// Prompts user for size, creates the square and prints it to console.
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args) {

            int size = RequestSizeFromUser();
            string square = CreateSquare(size);
            Console.WriteLine(square);
        }


        /// <summary>
        /// Prompts the user for a size for the square.
        /// Minimum value 3 in order to get atleast one space in the middle of the square
        /// </summary>
        /// <returns>The size the user has typed in.</returns>
        static int RequestSizeFromUser() {

            int size = 0; 

            Console.WriteLine("How big do you want the square to be? (min 3)");
            
            // In case the user is less gifted and types in a letter...
            try {
                size = int.Parse(Console.ReadLine());
            } catch(Exception e) {
                Console.WriteLine(e.Message);
            }

            if(size < 3) {
                Console.WriteLine("Minimum size is 3");
                RequestSizeFromUser();
            }

            return size;
        }

        /// <summary>
        /// Creates a string that represents the square, based on the size parameter.
        /// </summary>
        /// <param name="size"></param>
        /// <returns></returns>
        static string CreateSquare(int size) {
            string square = "";

            for (int i = 0; i < size; i++) {
                for (int j = 0; j < size; j++) {

                    // If the current (i, j) pair is on the border of the square, 
                    // add a hashtag, else add a space.
                    square += (i == 0 || i == size - 1 || j == 0 || j == size - 1) ? "#" : " ";

                    // If j reaches the end of the row, add a new line.
                    square += (j == size - 1) ? "\n" : "";
                }
            }

            return square;
        }
    }
}
